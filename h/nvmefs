/*
 * Copyright (c) 2023, Stader Softwareentwicklung GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stader Softwareentwicklung GmbH nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef nvmefs_h
#define nvmefs_h

/* Macro to silence compiler warnings about unused parameters */
#define IGNORE(x) do { (void)(x); } while(0)

/* Error numbers offset from our base */
#define ERR_NO_DISCOP          0
#define ERR_OP_TRANSFER_DATA   1
#define ERR_OP_SET_PRP_REF     2
#define ERR_OP_MEM_DISCOP      3
#define ERR_OP_MEM_CALC        4
#define ERR_ILLEGAL_DISC       5
#define ERR_NVM_OP_FAIL        6
#define ERR_SCATTER_LIST       7
#define ERR_NO_ROOM_BACKGROUND 8
#define ERR_DYNAMIC_AREA_SPACE 17
#define ERR_STRUCTURE_ASSERT   19
#define ERR_BAD_SUBREASON      20

/* Reason codes to DiscInfo */
#define NVMeFSDiscInfo_ReadDetails  0

/* Function which takes registers as passed by ShowFree, declared here but
 * implemented in assembler due to Free's unusual calling convention */
extern void show_free(void);

/* This function is autogenerated by ResGen, and returns a pointer to a
 * ResourceFS resource file data block */
extern void *Resources(void);

#endif
