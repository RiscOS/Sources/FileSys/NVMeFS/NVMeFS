/*
 * Copyright (c) 2005, Thomas Milius Stade
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stader Softwareentwicklung GmbH nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* !!!!!!!!!! includes !!!!!!!!!! */
#include <stddef.h>
#include <stdlib.h>
#include "kernel.h"
#include "swis.h"

#include "mod_mem_manag.h"

/* !!!!!!!!!! definitions !!!!!!!!!! */
#define DYNAMIC_AREA_HEAP_HEADER_SIZE 256
#define DYNAMIC_AREA_DEFAULT_EXPANSION_SIZE 8192

/* !!!!!!!!!! data structures !!!!!!!!!! */
static struct basic_settings_struct
{
  int dynamic_area_no;
  unsigned long expansion_size;
} basic_settings =
  {
  0,
  DYNAMIC_AREA_DEFAULT_EXPANSION_SIZE
  };

/* !!!!!!!!!! functions !!!!!!!!!! */
void mod_mem_manag_free(int dynamic_area_no,
                        void *ptr)
{
  _kernel_swi_regs regs;

  if (dynamic_area_no >= 0)
  {
    regs.r[0]=2;
    regs.r[1]=dynamic_area_no;
    _kernel_swi(OS_DynamicArea, &regs, &regs);
    regs.r[1]=regs.r[3];
    regs.r[0]=3;
    regs.r[2]=(int) ((unsigned long) ptr - 4);
    _kernel_swi(OS_Heap, &regs, &regs);
  }
  else
  {
    free(ptr);
  }
}

void mod_mem_manag_free_s(void *ptr)
{

  mod_mem_manag_free(basic_settings.dynamic_area_no,
                     ptr);
}

void *mod_mem_manag_aligned_alloc(int dynamic_area_no,
                                  size_t alignment,
                                  size_t size)
{
  _kernel_swi_regs regs;
  int heap_base;
  int *requested_size;

  if (dynamic_area_no >= 0)
  {
    /* Internal always a multiple of 4 */
    if ((size%4) != 0)
    {
      size+=(4 - (size%4));
    }
    /* Exit by certain situations later */
    while (true)
    {
      regs.r[0]=2;
      regs.r[1]=dynamic_area_no;
      _kernel_swi(OS_DynamicArea, &regs, &regs);
      heap_base=regs.r[3];
      regs.r[1]=heap_base;
      regs.r[2]=alignment;
      regs.r[0]=7;
      /* 4 additional bytes are required to store the size
         for allowing realloc */
      regs.r[3]=size + 4;
      regs.r[4]=0;
      if (_kernel_swi(OS_Heap, &regs, &regs) == NULL)
      {
        requested_size=(int *) regs.r[2];
        *requested_size=size;
        return (void *) ((unsigned long) regs.r[2] + 4);
      }
      else
      {
        /* Try to expand dynamic area:
           Note that Heap overhead can be big.
           So expand in given steps while maximum of area is not reached
           already */
        regs.r[0]=dynamic_area_no;
        regs.r[1]=DYNAMIC_AREA_DEFAULT_EXPANSION_SIZE;
        _kernel_swi(OS_ChangeDynamicArea, &regs, &regs);
        if (regs.r[1] == 0)
        {
          return NULL;
        }
        /* Expand Heap */
        regs.r[3]=regs.r[1];
        regs.r[0]=5;
        regs.r[1]=heap_base;
        _kernel_swi(OS_Heap, &regs, &regs);
      }
    }
  }
  else
  {
    return aligned_alloc(alignment,
                         size);
  }
}

void *mod_mem_manag_malloc(int dynamic_area_no,
                           size_t size)
{
  return mod_mem_manag_aligned_alloc(dynamic_area_no,
                                     0,
                                     size);
}

void *mod_mem_manag_malloc_s(size_t size)
{

  return mod_mem_manag_aligned_alloc(basic_settings.dynamic_area_no,
                                     0,
                                     size);
}


void *mod_mem_manag_aligned_alloc_s(size_t alignment,
                                    size_t size)
{

  return mod_mem_manag_aligned_alloc(basic_settings.dynamic_area_no,
                                     alignment,
                                     size);
}

void *mod_mem_manag_realloc(int dynamic_area_no,
                            void *ptr,
                            size_t size)
{
  _kernel_swi_regs regs;
  int heap_base;
  int *old_size;

  if (dynamic_area_no >= 0)
  {
    if (!ptr)
    {
      return mod_mem_manag_malloc(dynamic_area_no,
                                  size);
    }
    if (size == 0)
    {
      mod_mem_manag_free(dynamic_area_no,
                         ptr);
      return ptr;
    }
    /* Internal always a multiple of 4 */
    if ((size%4) != 0)
    {
      size+=(4 - (size%4));
    }
    old_size=(int *) ((unsigned long) ptr - 4);
    if ((size - *old_size) == 0)
    {
      /* No change */
      return ptr;
    }
    /* Exit by certain situations later */
    while (true)
    {
      regs.r[0]=2;
      regs.r[1]=dynamic_area_no;
      _kernel_swi(OS_DynamicArea, &regs, &regs);
      heap_base=regs.r[3];
      regs.r[1]=heap_base;
      regs.r[0]=4;
      regs.r[2]=(int) old_size;
      /* Because determining byte change there is no need
         to worry about 4 Byte size storage */
      regs.r[3]=size - *old_size;
      if (_kernel_swi(OS_Heap, &regs, &regs) == NULL)
      {
        old_size=(int *) regs.r[2];
        *old_size=size;
        return (void *) ((unsigned long) regs.r[2] + 4);
      }
      else
      {
        /* Try to expand dynamic area:
           Note that Heap overhead can be big.
           So expand in given steps while maximum of area is not reached
           already */
        regs.r[0]=dynamic_area_no;
        regs.r[1]=DYNAMIC_AREA_DEFAULT_EXPANSION_SIZE;
        _kernel_swi(OS_ChangeDynamicArea, &regs, &regs);
        if (regs.r[1] == 0)
        {
          return NULL;
        }
        /* Expand Heap */
        regs.r[3]=regs.r[1];
        regs.r[0]=5;
        regs.r[1]=heap_base;
        _kernel_swi(OS_Heap, &regs, &regs);
      }
    }
  }
  else
  {
    return realloc(ptr,
                   size);
  }
}

void *mod_mem_manag_realloc_s(void *ptr,
                              size_t size)
{

  return mod_mem_manag_realloc(basic_settings.dynamic_area_no,
                               ptr,
                               size);
}

int mod_mem_manag_dynamic_area_initialize(char *area_name,
                                          unsigned long max_area_size,
                                          unsigned long expansion_size)
{
  _kernel_swi_regs regs;
  int new_area_number;
  void *base_of_area;

  /* test for dynamic area support */
  if (expansion_size == 0)
  {
    expansion_size=DYNAMIC_AREA_DEFAULT_EXPANSION_SIZE;
  }
  regs.r[0]=3;
  regs.r[1]=-1;
  if (_kernel_swi(OS_DynamicArea, &regs, &regs) != NULL)
  {
    /* No Dynamic Area so take RMA for memory allocation */
    return -1;
  }
  regs.r[0]=0;
  regs.r[1]=-1;
  regs.r[2]=(int) expansion_size;
  regs.r[3]=-1;
  regs.r[4]=0x000000B0;
  regs.r[5]=(int) max_area_size;
  regs.r[6]=NULL;
  regs.r[7]=NULL;
  regs.r[8]=(int) area_name;
  _kernel_swi(OS_DynamicArea, &regs, &regs);
  new_area_number=regs.r[1];
  base_of_area=(void *) regs.r[3];
  /* Determine true actual size */
  regs.r[0]=2;
  _kernel_swi(OS_DynamicArea, &regs, &regs);
  /* Initialize the Heap */
  regs.r[0]=0;
  regs.r[1]=(int) base_of_area;
  regs.r[3]=regs.r[2] - DYNAMIC_AREA_HEAP_HEADER_SIZE;
  _kernel_swi(OS_Heap, &regs, &regs);
  /* keep the last settings as standard memory defaults */
  basic_settings.dynamic_area_no=new_area_number;
  basic_settings.expansion_size=expansion_size;
  return new_area_number;
}

bool mod_mem_manag_dynamic_area_release(int dynamic_area_no)
{
  _kernel_swi_regs regs;

  if (dynamic_area_no < 0) return true;
  /* No Need to release the heap for it is dropped with the area */
  /* Release the Area */
  regs.r[0]=1;
  regs.r[1]=dynamic_area_no;
  _kernel_swi(OS_DynamicArea, &regs, &regs);
  /* reset the settings for standard memory defaults */
  basic_settings.dynamic_area_no=0;
  basic_settings.expansion_size=DYNAMIC_AREA_DEFAULT_EXPANSION_SIZE;
  return true;
}
